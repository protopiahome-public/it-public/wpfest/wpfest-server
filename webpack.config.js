var path = require('path');
var Webpack = require('webpack')
var NpmInstallPlugin = require("webpack-plugin-install-deps")

module.exports =  {
    mode: 'development',
    devtool: 'cheap-module-eval-source-map',
    entry: [
      'webpack-hot-middleware/client',
      './src/index.js'
    ],
    module: {
      rules: [
          {
              test: /\.mjs$/,
              include: /node_modules/,
              type: 'javascript/auto',
          },
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          query: {
            presets: ["react", "es2015", "stage-0"]
          }
        },
        // {
        //   test: /\.js$/,
        //   include: [
        //     /node_modules\/react-native-/,
        //   ],
        //   use: {
        //     loader: 'babel-loader',
        //     options: {
        //       presets: ['react-native'],
        //     },
        //   },
        // },
          // test: /postMock.html$/,
          // use: {
          //   loader: 'file-loader',
          //   options: {
          //     name: '[name].[ext]',
          //   },
          // },
          // },
          {
          test: /\.(graphql|gql)$/,
          exclude: /node_modules/,
          loader: 'graphql-tag/loader',
        },
        {
          test: /\.html$/,
          use: 'raw-loader'
        },
      ]
    },
    resolve: {
      extensions: ['*', '.js', '.jsx'],
      alias: {
        // 'react-native': 'react-web',
        // 'WebView': 'react-native-web-webview',
      }
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.js',
      // publicPath: 'public'
    },
    plugins: [
          new Webpack.HotModuleReplacementPlugin(),
          new Webpack.NoEmitOnErrorsPlugin(),
          new NpmInstallPlugin()
        ],
  }

  //https://maxfarseer.gitbooks.io/redux-course-ru/content/es2015,_react_hmr.html