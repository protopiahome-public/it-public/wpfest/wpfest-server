import {ApolloServer} from "apollo-server-hapi";
import {Server} from "hapi";
import schema from './schema.js';
import server from "./config/server_config";

export default async function (msg, ctx) {

    console.log("Welcome to Ecosystem");

        //
    try {
        const apolloServer = new ApolloServer(schema(msg, ctx));

        const app = new Server({
            host: server.host,
            port: server.port,
            "routes": {
                "cors": {
                    origin: ["*"],
                    headers: ["Accept", "Content-Type",  "id_token", "authorization"],
                    additionalHeaders: ["X-Requested-With"]
                }
            }
        });

        //await
        await apolloServer.applyMiddleware({
            app,
        });

        //await
        await apolloServer.installSubscriptionHandlers(app.listener);


        //await
        await app.start();
    }catch (e) {
        console.log(e);
    }

        //config





    };