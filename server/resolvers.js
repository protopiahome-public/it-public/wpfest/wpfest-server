import {GraphQLDate, GraphQLDateTime} from 'graphql-iso-date';
import {URL, EmailAddress} from '@okgrow/graphql-scalars';
import { readdirSync, statSync } from 'fs';
const path = require('path');


var queries = {};
// require("fs").readdirSync(__dirname + "/queries").forEach(function(file) {
//   var query = require(__dirname + "/queries/" + file);
//   for (var key in query) {
// 	  queries[key] = query[key];
//   }
// });

var mutations = {};
// require("fs").readdirSync(__dirname + "/mutations").forEach(function(file) {
//   var mutation = require(__dirname + "/mutations/" + file);
//   for (var key in mutation) {
// 	  mutations[key] = mutation[key];
//   }
// });

var resolvers = {
    EmailAddress,
    URL,
    DateTime: GraphQLDateTime,

};


const dir = __dirname + "/module";
const files = readdirSync(dir);
for (const file of files) {
    const stat = statSync(path.join(dir, file));
    if (stat.isDirectory()){
        const resolver_list = require(path.join(dir, file, "resolvers.js"));
		for (var key in resolver_list) {
			if (key != 'mutation' && key != 'query') {
				resolvers[key] = resolver_list[key];
			}
		}
        for (var key in resolver_list.mutation) {
            mutations[key] = resolver_list.mutation[key];
        }
        for (var key in resolver_list.query) {
            queries[key] = resolver_list.query[key];
        }
    }
}

//https://gist.github.com/kethinov/6658166


resolvers.Query = queries;
resolvers.Mutation = mutations;


export default resolvers;


