import collection from "./collection";
import collectionItem from "./collectionItem";
import configCollection from "./configCollection";

const { spawnStateless} = require('nact');


export default function(serverActor){

    spawnStateless(serverActor, collection, "collection");
    spawnStateless(serverActor, collectionItem, "item");
    spawnStateless(serverActor, configCollection, "config_collection");


}
