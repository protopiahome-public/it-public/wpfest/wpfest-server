const { dispatch } = require('nact');

var fs = require('fs');
const path = require('path');

export default async function(msg, ctx)
{

    const type = msg.type.slice();
    const file = type + ".json";
    delete (msg.type);
    const dir = __dirname + "/../config";

    let config;
    if(msg.input){
        //type
        const data = fs.readFileSync(path.join(dir, file), 'utf8');

        const obj = JSON.parse(data); //now it an object
        obj.push(msg.input); //add some data
        config = JSON.stringify(obj); //convert it back to json
        fs.writeFileSync(path.join(dir, file), config, 'utf8'); // write it back
    }else{

        config = require(path.join(dir, file));

    }


    dispatch(ctx.sender,  config, ctx.self);

    // await db.client.findOne({clientSecret: authorizationArray[1]});
}