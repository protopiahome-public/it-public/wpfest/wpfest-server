import db from "../db";

import {ObjectId} from "promised-mongo";

const { dispatch } = require('nact');

//Stateless - Сохраняющий свое состояние во внешний источник.

const resource = "client";

export default async function(msg, ctx)
{

    const type = msg.type.slice();
    delete (msg.type);


    const collection =  db.collection(type).aggregate([{"$sort" : {"title" : 1}}]);



    dispatch(ctx.sender,  collection, ctx.self);

    // await db.client.findOne({clientSecret: authorizationArray[1]});
}