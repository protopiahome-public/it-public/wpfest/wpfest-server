import jws from "jws";


//Stateless - Сохраняющий свое состояние во внешний источник.

// iss (эмитент) — обязательное поле. Адрес HTTPS: URI с указанием полного имени хоста эмитента, который в паре с user_id, создает глобально уникальный и никогда непереназначаемый идентификатор. Например, "https://aol.com", "https://google.com", или "https://sakimura.org".
// sub — обязательное поле. Локально уникальный и никогда непереназначаемый идентификатор для пользователя (субъекта). Например, "24400320"
// aud (аудитория) — обязательное поле. Идентификатор клиента (сlient_id) для которого, этот id_token предназначен.
// ехр (окончание) — обязательное поле. Время, после которого не может быть принят этот маркер.
// nonce — обязательное поле. Установленное сервером значение отправленное в запросе.

/**
 * Generate a JWT
 *
 * @param $privateKey The private key to use to sign the token
 * @param $iss The Issuer, usually the URI for the oauth server
 * @param $sub The Subject, usually a user_id
 * @param $aud The Audience, usually client_id
 * @param $exp The Expiration date. If the current time is greater than the exp, the JWT is invalid
 * @param $iat The "Issued at" date.
 * @param $nbf The "not before" time. If the current time is less than the nbf, the JWT is invalid
 * @param $jti The "jwt token identifier", or nonce for this JWT
 *
 * @return string
 */
const url = "https://ecosystem.protopia.com";
const idTokenLifetime = 60 * 60;
module.exports = {
    generate: (client_id, user_id, user, issued_at, claims, client_secret) => {

        // user.filer(claims);

        const expires_at = issued_at + idTokenLifetime;

        const id_token = jws.sign({
            header: { alg: 'HS256' },
            payload:             {
                sub: user_id,
                aud: client_id,
                iss: url,
                iat: issued_at,
                exp: expires_at,

            },
            secret: client_secret,
        })
        return id_token;

    },
    verify: (id_token, client_secret) => {
        return jws.verify(id_token, 'HS256', client_secret);
    },
    decode: (id_token) => {
        return jws.decode(id_token);
    },
    expires_in: (time) => {
        return time + idTokenLifetime;
    }
}

//
// //                {
// //                     "iss": "t.me",
// //                     "sub": telegram_user_id,
// //                     "aud": server_id,
// //                     "exp": "",
// //                     "nonce": ""
// //                 }
//
//


//
// //https://openid.net/specs/openid-connect-core-1_0.html#CodeFlowAuth
// // {
// //    "iss": "https://server.example.com",
// //    "sub": "24400320",
// //    "aud": "s6BhdRkqt3",
// //    "nonce": "n-0S6_WzA2Mj",
// //    "exp": 1311281970,
// //    "iat": 1311280970,
// //    "auth_time": 1311280969,
// //    "acr": "urn:mace:incommon:iap:silver"
// //   }