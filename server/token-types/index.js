import idTokenType from "./idTokenType";
import accessTokenType from "./accessTokenType";
import refreshTokenType from "./refreshTokenType";

module.exports = {

    idTokenType: idTokenType,
    accessTokenType: accessTokenType,
    refreshTokenType: refreshTokenType
}