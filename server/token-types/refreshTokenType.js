import jws from "jws";

//Stateless - Сохраняющий свое состояние во внешний источник.


/**
 * Generate a JWT
 *
 * @param $privateKey The private key to use to sign the token
 * @param $iss The Issuer, usually the URI for the oauth server
 * @param $sub The Subject, usually a user_id
 * @param $aud The Audience, usually client_id
 * @param $exp The Expiration date. If the current time is greater than the exp, the JWT is invalid
 * @param $iat The "Issued at" date.
 * @param $nbf The "not before" time. If the current time is less than the nbf, the JWT is invalid
 * @param $jti The "jwt token identifier", or nonce for this JWT
 *
 * @return string
 */
const refreshTokenLifetime = 60 * 60 * 24 * 14;  // 2 weeks.
const url = "https://ecosystem.protopia.com";
module.exports = {
    generate: async (client_id, user_id, scope, issued_at, client_secret) => {
        const expires_at = issued_at + refreshTokenLifetime;

        const refresh_token = jws.sign({
            header: { alg: 'HS256' },
            payload: {
                sub: user_id,
                aud: client_id,
                iss: url,
                iat: issued_at,
                exp: expires_at,

                client_id: client_id,
                scp: scope
            },
            secret: client_secret,
        })

        return  refresh_token;

    },
    verify: (refresh_token, client_secret) => {
        return jws.verify(refresh_token, 'HS256', client_secret);
    },
    decode: (refresh_token) => {
        return jws.decode(refresh_token);
    },
    expires_in: (time) => {
        return time + refreshTokenLifetime;
    }
}

// //https://dajbych.net/stateless-stateful-or-actor-service