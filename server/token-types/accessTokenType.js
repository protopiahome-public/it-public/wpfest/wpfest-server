import jws from "jws";

const uuidv1 = require('uuid/v1');


/**
 * Generate a JWT
 *
 * @param $privateKey The private key to use to sign the token as client_secret
 * @param $iss The Issuer, usually the URI for the oauth server
 * @param $sub The Subject, usually a user_id
 * @param $aud The Audience, usually client URI
 * @param $exp The Expiration date. If the current time is greater than the exp, the JWT is invalid
 * @param $iat The "Issued at" date.
 * @param $nbf The "not before" time. If the current time is less than the nbf, the JWT is invalid
 * @param $jti The "jwt token identifier", or nonce for this JWT
 *
 * @return string
 */

const url = "https://ecosystem.protopia.com";

// generate
// verify

// accessTokenLifetime: 60 * 60,       // 1 hour.
const accessTokenLifetime = 60 * 60;

//    const now = new Date();
//     const issued_at = now.getTime();
//     const expiredDate = new Date(now + 1);
//     const expires_at = expiredDate.getTime();

module.exports = {
    generate: (client_id, user_id, scope, issued_at, client_secret) => {

        // client_secret
        const expires_at = issued_at+accessTokenLifetime;

        const access_token = jws.sign({
            header: { alg: 'HS256' },
            payload: {
                sub: user_id,
                aud: client_id,
                iss: url,
                iat: issued_at,
                exp: expires_at,

                sid: uuidv1(),

                client_id: client_id,
                scp: scope
                },
            secret: client_secret,
        })

        return  access_token;

    },
    verify: (access_token, client_secret) => {
        return jws.verify(access_token, 'HS256', client_secret);
    },
    decode: (access_token) => {
        return jws.decode(access_token);
    },
    expires_in: (time) => {
        return time + accessTokenLifetime;
    }
}

//https://medium.com/@darutk/oauth-access-token-implementation-30c2e8b90ff0

