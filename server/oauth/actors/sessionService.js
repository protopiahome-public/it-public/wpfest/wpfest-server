import db from "../../db";
import {ObjectId} from "promised-mongo";

import {host, port} from "../../config/server_config"


import { AccessControl } from 'role-acl';
import grants from "../../config/grants";

const access = new AccessControl(grants);
const { dispatch, spawn, stop } = require('nact');

//пользователь дает клиенту разрешения.
//"role", "resource", "action", "attributes"
//"role" - client.app_type or user.roles
//"resource" - server (client.app_type) (such as resources, computers, and applications)

// grants может получить как клиент, так и пользователь
// grants для клиента определяются его app_type
// grants для пользователя определяются его ролями
// пользователь может передавать через scope свои grants пользователю.

// Scopes
// A scope is the smallest entity that describes a single permission.
// Roles
// A role is a collection of multiple scopes (permissions) that can be assigned to a user or another role.

// access.grant('user').condition(
//     (context) => {
//         return ctx.user.groups.filter(value => ctx.resource.groups.includes(value))
//     }
// ).execute('read').on('theme');

//TODO пока не универсальный.

/**
 * Generate a JWT
 *
 * @param $iss The Issuer, usually the URI for the oauth server
 * @param $sub The Subject, usually a user_id
 * @param $aud The Audience, usually client URI
 * @param $exp The Expiration date. If the current time is greater than the exp, the JWT is invalid
 * @param $iat The "Issued at" date.
 * @param $nbf The "not before" time. If the current time is less than the nbf, the JWT is invalid
 * @param $jti The "jwt token identifier", or nonce for this JWT
 *
 */

//shutdownAfter: 10 * minutes


export default (parent, sessionId, initalState) => spawn(
    parent,
    async (
        state =
            {
                token:{
                    sub: "",
                    aud: "",
                    iss: "",
                    iat: "",
                    exp: "",

                    sid: "",

                    client_id: "",
                    scp: ""
                },

                client:{
                    application_type: "",
                    client_secret: "",
                },

                user:{
                    roles: ""
                }

            },
        msg, ctx) =>
    {

        let token = state.token;
        let client = state.client;
        let user = state.user;

        if(new Date().toTimeString() > state.exp ){

            stop(ctx.self);
            throw new AuthenticationError('Time is incorrect');
        }

        if(!client){

            if(token.client_id){
                client = await db.client.findOne({
                    query: {_id: new ObjectId(token.client_id)}
                });

                state.client.client_secret = client.client_secret;
                state.client.application_type = client.application_type;

                if(!client)
                    throw new AuthenticationError('Must authenticate');

                user = await db.user.findOne({
                    query: {_id: new ObjectId(token.sub)}
                });

                state.user.roles = user.roles;

            }else{
                client = await db.client.findOne({
                    query: {_id: new ObjectId(token.sub)}
                });

                state.client.client_secret = client.client_secret;
                state.client.application_type = client.application_type;

                //user undefined
            }

        }

        // accessTokenType.verify(token, state.client_secret)
        if(msg.client_secret !== client.client_secret)
            throw new AuthenticationError('Signature incorrect');

        if(msg.host !== token.iss || host !== token.iss){
            throw new AuthenticationError('URL incorrect');
        }

        if(msg.host !== token.aud){
            throw new AuthenticationError('URL incorrect');
        }

        if(client.application_type && user.roles){
            access.grant('admin').extend('user');
            access.grant(user.roles).extend(state.application_type);
        }

        // const scope = token.scp;
        //not client_creditals
        // scope.forEach( (e) => {
        //     access.grant(client.app_type).execute(e).on('*')
        // } );


        const permission = await access.permission({
            role: client.application_type,
            resource: "",
            action: msg.query_name
            // , context: context
        });

        if(!permission.granted)
            throw new ForbiddenError('Resource Forbidden');

        dispatch(ctx.sender, state.token, ctx.self);
        return state;
    },
    sessionId,
    {initalState: initalState}
);

// throw new AuthorizationError('you must be logged in to query this schema');

//https://www.npmjs.com/package/role-acl
//https://onury.io/accesscontrol/?content=faq
//https://developer.mindsphere.io/concepts/concept-roles-scopes.html
//https://wso2.com/library/articles/2015/12/article-role-based-access-control-for-apis-exposed-via-wso2-api-manager-using-oauth-2.0-scopes/
//https://onury.io/accesscontrol/