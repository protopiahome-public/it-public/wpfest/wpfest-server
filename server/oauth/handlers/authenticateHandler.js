import getQueryName from "../helpers/getQueryName";
import accessTokenType from "../../token-types/accessTokenType";

import sessionService from "../actors/sessionService"

const {AuthenticationError} = require('apollo-server');

import {InvalidTokenError} from "../errors";

const { query, dispatch } = require('nact');

export default async (request, h, ctx) => {

    const headers = request.headers;
    const host = request.info.host;

    const authorization = headers.authorization || false;

    if (!authorization)
        throw new AuthenticationError('Has not Authorization Header');

    const authorizationArray = authorization.split(' ');

    //token.match(/Bearer\s(\S+)/);
    if (authorizationArray[0] !== 'Bearer')
        throw new AuthenticationError('It is not Bearer Type');

    let sid;
    try {
        const token = accessTokenType.decode(authorizationArray[1]);
        sid = token.payload.sid;
        const client_secret = token.signature;

        let childActor;
        if(ctx.children.has(sid)){
            childActor = ctx.children.get(sid);
        } else {
            childActor = sessionService(ctx.self, sid, {token: token.payload});
        }

        dispatch(childActor, {host: host, query_name: getQueryName(request), client_secret: client_secret}, ctx.sender);

    }catch (e) {
        console.log(e);
        throw new InvalidTokenError('Invalid Token');
    }

    return sid;

}


// права метода
// if (name) {
//     h.set('X-Accepted-OAuth-Scopes', name);
// }

// Согласие пользователя this.addAuthorizedScopesHeader
// if (scope) {
//     h.set('X-OAuth-Scopes', scope);
// }