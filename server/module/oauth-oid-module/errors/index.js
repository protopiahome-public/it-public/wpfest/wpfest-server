// https://www.apollographql.com/docs/apollo-server/features/errors/

const {ApolloError} = require('apollo-server');

export class InvalidRequestError extends ApolloError {
    constructor(message: string) {
        super(message, 'INVALID_REQUEST');

        Object.defineProperty(this, 'name', { value: 'InvalidRequestError' });
    }
}

export class InvalidGrantError extends ApolloError {
    constructor(message: string) {
        super(message, 'INVALID_GRANT');

        Object.defineProperty(this, 'name', { value: 'InvalidGrantError' });
    }
}

export class ExpiredTokenError extends ApolloError {
    constructor(message: string) {
        super(message, 'EXPIRED_TOKEN');

        Object.defineProperty(this, 'name', { value: 'ExpiredTokenError' });
    }
}



export class UnauthorizedClientError extends ApolloError {
    constructor(message: string) {
        super(message, 'UNAUTHORIZED_CLIENT');

        Object.defineProperty(this, 'name', { value: 'UnauthorizedClientError' });
    }
}



export class AccessDeniedError extends ApolloError {
    constructor(message: string) {
        super(message, 'ACCESS_DENIED');

        Object.defineProperty(this, 'name', { value: 'AccessDeniedError' });
    }
}

export class UnknownUserIDError extends ApolloError {
    constructor(message: string) {
        super(message, 'UNKNOWN_USER_ID');

        Object.defineProperty(this, 'name', { value: 'UnknownUserIDError' });
    }
}

export class UnknownAuthReqIdError extends ApolloError {
    constructor(message: string) {
        super(message, 'UNKNOWN_AUTH_REQ_ID');

        Object.defineProperty(this, 'name', { value: 'UnknownAuthReqIdError' });
    }
}

export class MissingUserCodeError extends ApolloError {
    constructor(message: string) {
        super(message, 'MISSING_USER_CODE');

        Object.defineProperty(this, 'name', { value: 'MissingUserCodeError' });
    }
}

export class InvalidUserCodeError extends ApolloError {
    constructor(message: string) {
        super(message, 'INVALID_USER_CODE');

        Object.defineProperty(this, 'name', { value: 'InvalidUserCodeError' });
    }
}

export class AuthorizationPendingError extends ApolloError {
    constructor(message: string) {
        super(message, 'AUTHORIZATION_PENDING');

        Object.defineProperty(this, 'name', { value: 'AuthorizationPendingError' });
    }
}

export class SlowDownError extends ApolloError {
    constructor(message: string) {
        super(message, 'SLOW_DOWN');

        Object.defineProperty(this, 'name', { value: 'SlowDownError' });
    }
}



//export class SyntaxError extends ApolloError {
//   constructor(message: string) {
//     super(message, 'GRAPHQL_PARSE_FAILED');
//
//     Object.defineProperty(this, 'name', { value: 'SyntaxError' });
//   }
// }