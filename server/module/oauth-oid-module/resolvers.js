import {associateHandler, authorizationHandler, tokenHandler,  revokeHandler, introspectHandler } from "./actors/index2";

const { query } = require('nact');

module.exports = {

    mutation: {
        register: async (obj, args, ctx, info) => {

            // const {collectionItemActor} = require("../../../actorSystem");
            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {"type": "client", input: args.input}, 250);

        },

        updateClient: async (obj, args, ctx, info) => {

            // const {collectionItemActor} = require("../../../actorSystem");
            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {
                "type": "client",
                search: {_id: args._id},
                input: args.input
            }, 250);

        },

        removeClient: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const {collectionItemActor} = require("../../../actorSystem");

            return await query(collectionItemActor, {
                "type": "client",
                search: {_id: args._id},
                input: {is_remove: true}
            }, 250);

        },

        associate: async (obj, args, ctx, info) => {

            return associateHandler(args.input, ctx);

        },

        authorize: async (obj, args, ctx, info) => {

            return authorizationHandler(args.input, ctx);

        },

        token: async (obj, args, ctx, info) => {

            return tokenHandler(args.input, ctx);

        },

        revoke: async (obj, args, ctx, info) => {

            return revokeHandler(args.input,ctx);

        }
    },
    query:{
        getClients: async (obj, args, ctx, info) => {

            // const {collectionActor}  = require("../../../actorSystem");
            const collectionActor = ctx.children.get("collection");

            return await query(collectionActor,  {"type": "client"}, 250);

        },

        getClient: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const {collectionItemActor}  = require("../../../actorSystem");

            return await query(collectionItemActor,  {"type": "client", _id: args._id}, 250);

        },


        introspect: async (obj, args, ctx, info) => {

            return introspectHandler(args.input,ctx);

        },
    }
}