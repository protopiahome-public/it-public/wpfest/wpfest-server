import accessTokenType from "../../../token-types/accessTokenType";
import refreshTokenType from "../../../token-types/refreshTokenType";
import idTokenType from "../../../token-types/idTokenType";

export default async function(args, ctx) {

    const client = ctx.client;

    const token = args.token;
    const token_type_hint = args.token_type_hint;

    switch (token_type_hint) {
        case "refresh_token":
            return refreshTokenType.verify(token, client.client_secret);
            break;
        case "access_token":
            return accessTokenType.verify(token, client.client_secret);
            break;
        case "id_token":
            return idTokenType.verify(token, client.client_secret);
            break;
        default:
            break;
    }

}
