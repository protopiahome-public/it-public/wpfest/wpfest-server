import tokenResponse from "../response/tokenResponse";
import accessTokenType from "../../../../token-types/accessTokenType";
import refreshTokenType from "../../../../token-types/refreshTokenType";



const {ApolloError, AuthenticationError, ForbiddenError} = require('apollo-server');

export default async function(args, ctx) {

    const client = ctx.client;

    const collectionItemActor = ctx.children.get("item");

    let authenticator =  await query(collectionItemActor, {"type": "authenticate_session", search: {"auth_req_id": args.auth_req_id} }, 250);

    if (!authenticator) {
        throw new ApolloError('Incorrect password')
    }

    const now = new Date();
    const issued_at = now.getTime();

    const access_token = accessTokenType.generate(client._id, args.user._id, args.scope, issued_at);
    const expires_at = accessTokenType.expires_in(issued_at);
    const refresh_token = refreshTokenType.generate(client._id, args.user._id, args.scope, issued_at);


    return tokenResponse(access_token, expires_at, refresh_token, code.scope);
}