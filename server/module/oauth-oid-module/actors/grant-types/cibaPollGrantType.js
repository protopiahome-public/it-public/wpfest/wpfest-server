import tokenResponse from "../response/tokenResponse";
import accessTokenType from "../../../../token-types/accessTokenType";
import idTokenType from "../../../../token-types/idTokenType";
import refreshTokenType from "../../../../token-types/refreshTokenType";

import {UnknownAuthReqIdError} from "../../errors";

export default async function(args, ctx) {

    const client = ctx.client;

    const collectionItemActor = ctx.children.get("item");

    let authenticate_session =  await query(collectionItemActor, {"type": "authenticate_session", search: {"auth_req_id": args.auth_req_id} }, 250);

    if (!authenticate_session) {
        throw new UnknownAuthReqIdError('Unknown auth_req_id');
    }

    const client_id = authenticate_session.client_id;
    const user_id = authenticate_session.user_id;
    const scope = authenticate_session.scope;

    const now = new Date();
    const issued_at = now.getTime();

    let id_token;
    if(scope === "openid"){
        //const claims = scopesToClaims(scope);
        id_token = idTokenType.generate(client._id, user_id,"","");
    }

    const access_token = accessTokenType.generate(client_id, user_id, scope, issued_at);
    const expires_at = accessTokenType.expires_in(issued_at);
    const refresh_token = refreshTokenType.generate(client_id, user_id, scope, issued_at);


    return tokenResponse(access_token, expires_at, refresh_token, scope, id_token);
}