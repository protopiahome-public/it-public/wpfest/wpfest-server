import tokenResponse from "../response/tokenResponse";
import accessTokenType from "../../../../token-types/accessTokenType";
import refreshTokenType from "../../../../token-types/refreshTokenType";

// grant_type=refresh_token
// &client_id=xxxxxxxxxx
// &client_secret=xxxxxxxxxx
// &refresh_token=xxxxxxxxxxx

const {ApolloError, AuthenticationError, ForbiddenError} = require('apollo-server');

export default async function(args, ctx) {

    const client = ctx.client;

    const refreshToken = refreshTokenType.decode(args.refresh_token);

    if(!(client.id === refreshToken.client_id))
        throw new AuthenticationError('');

    const now = new Date();
    const issued_at = now.getTime();

    const access_token = accessTokenType.generate(refreshToken.iss, refreshToken.sub, refreshToken.scope, issued_at);
    const expires_at = accessTokenType.expires_in(issued_at);
    const refresh_token = refreshTokenType.generate(refreshToken.iss, refreshToken.sub, refreshToken.scope, issued_at);

    return tokenResponse(access_token, expires_at, refresh_token, refreshToken.scope);

}