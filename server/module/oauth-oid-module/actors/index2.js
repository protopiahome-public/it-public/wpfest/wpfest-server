import associateHandler from "./associateHandler";
import authorizationHandler from "./authorizationHandler";
import tokenHandler from "./tokenHandler";
import introspectHandler from "./introspectHandler";
import revokeHandler from "./revokeHandler";

module.exports = {

    associateHandler: associateHandler,
    authorizationHandler: authorizationHandler,
    tokenHandler: tokenHandler,
    introspectHandler: introspectHandler,
    revokeHandler: revokeHandler

}
