import idTokenType from "../../../../token-types/idTokenType";
import bcrypt from "bcrypt";

import {UnknownUserIDError, ExpiredTokenError, AccessDeniedError, InvalidUserCodeError} from "../../errors";

const { query } = require('nact');

//https://www.npmjs.com/package/jwt-pwd

export default async function(id_hint_token, user_code, ctx) {

    const collectionItemActor = ctx.children.get("item");

    const idToken = idTokenType.decode(id_hint_token); //id_token

    if(new Date().toTimeString() > idToken.exp ){
        throw new ExpiredTokenError("Token Expired");
    }

    let user;

    switch (idToken.acr) {
        case "leaderid":
            user = await query(collectionItemActor, {type: "user", search: {"leader_id": idToken.sub}  }, 250);
            break;
        case "telegram":
            user = await query(collectionItemActor, {type: "user", search: {"telegram_id": idToken.sub}  }, 250);
            break;
        case "vk":
            user = await query(collectionItemActor, {type: "user", search: {"vk_id": idToken.sub}  }, 250);
            break;
        case "slack":
            user = await query(collectionItemActor, {type: "user", search: {"slack_id": idToken.sub}  }, 250);
            break;
        case "email":
            user = await query(collectionItemActor, {type: "user", search: {"email": idToken.sub}  }, 250);
            break;
        default:
            break;
    }

    if (!user) {
        throw new UnknownUserIDError('No user')
    }

    let authenticator;
    switch (idToken.amr) {
        case "opt":
            authenticator =  await query(collectionItemActor, {"type": "authenticate_session", search: {"user_code": user_code} }, 250);
            if (!authenticator) {
                throw new InvalidUserCodeError('Incorrect one time password')
            }
            break;
        case "mca":
            authenticator =  await query(collectionItemActor, {type: "authenticate_session", search: {"user_code": user_code} }, 250);
            if (authenticator){
                switch (authenticator.oob_channel) {
                    case "slack":
                        user = await query(collectionItemActor, {type: "user", search: {"_id": authenticator.user_id}, input:{slack_id: idToken.sub}  }, 250);
                        break;
                    case "telegram":
                        user = await query(collectionItemActor, {type: "user", search: {"_id": authenticator.user_id} , input:{telegram_id: idToken.sub}  }, 250);
                        break;
                    case "vk":
                        user = await query(collectionItemActor, {type: "user", search: {"_id": authenticator.user_id} , input:{vk_id: idToken.sub}  }, 250);
                        break;
                }
            } else{
                throw new InvalidUserCodeError('Incorrect user code')
            }
            break;
        case "pwd":
            const valid = bcrypt.compareSync(idToken.pwd, user.crypto_password);
            if (!valid) {
                throw new InvalidUserCodeError('Incorrect password')
            }
            break;
        default:
            break;
    }

    return user;

}

//https://www.npmjs.com/package/jwt-otp