import {UnknownUserIDError} from "../../errors";

const { query } = require('nact');

export default async function(user_code,  ctx) {

    const collectionItemActor = ctx.children.get("item");

    let user;

    try {
        const  authenticator =  await query(collectionItemActor, {type: "authenticate_session", search: {"user_code": user_code} }, 250);

        user = await query(collectionItemActor, {type: "user", search: {"_id": authenticator.user_id}  }, 250);

    }catch (e) {

        throw new UnknownUserIDError('Incorrect user code')
    }

    return user;

}