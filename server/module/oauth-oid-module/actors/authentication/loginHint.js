import bcrypt from "bcrypt";

const { query } = require('nact');

import {UnknownUserIDError} from "../../errors";

export default async function(login_hint, user_code, ctx) {

    const collectionItemActor = ctx.children.get("item");

    const user = await query(collectionItemActor, {type: "user", search: {"email": login_hint}  }, 250);

    if (!user) {
        throw new UnknownUserIDError('No user with that email')
    }

    let authenticator =  await query(collectionItemActor, {"type": "authenticate_session", search: {"user_code": user_code} }, 250);


    if(!authenticator){
        const valid = bcrypt.compareSync(user_code, user.crypto_password);
    }



    return user;

}