import loginHint from "./authentication/loginHint";
import loginUserCode from "./authentication/loginUserCode";
import loginHintToken from "./authentication/loginHintToken";
import authIDFlow from "./flows/authIDFlow";
import authorizeResponse from "./response/authorizeResponse";

import {
    AccessDeniedError,
    InvalidScopeError,
    MissingUserCodeError,
    UnauthorizedClientError
} from "../errors";

// const { query } = require('nact');

export default async function(args, ctx) {

    const client = ctx.client;

    if(!client){
        throw new UnauthorizedClientError("client not authorized");
    }

    const scope = args.scope ? args.scope.split(" ") : [];
    //InvalidScopeError
    //AccessDeniedError

    const login_hint_token = args.login_hint_token;
    const id_hint_token = args.id_hint_token;
    const login_hint = args.login_hint;

    const user_code = args.user_code;

    if(!user_code){
        throw new MissingUserCodeError("missing user code");
    }

    let user;

    if(login_hint_token) {
        user = await loginHintToken(login_hint_token, user_code, ctx);

    }else if(id_hint_token ){
        user = await loginHintToken(id_hint_token, user_code, ctx);
    }
    else if(login_hint && user_code){
        user = await loginHint(login_hint, user_code, ctx);
    }else if (user_code){
        user = await loginUserCode(user_code, ctx);
    }

    const interval = 25;
    const expires_in = now.getTime() + interval;

    const auth_req_id = authIDFlow(client, user, scope, ctx);

    return authorizeResponse(auth_req_id, expires_in, interval, args.state);

}