const {AuthenticationError, ForbiddenError} = require('apollo-server');
// import {AuthenticationError} from 'apollo-server';
import {ObjectId} from "promised-mongo";

const resource = "project";

module.exports = {

    mutation:{
        changeProject: async (obj, args, context, info) => {

            const {collectionItemActor}  = require("../../../actorSystem");

            if(args._id){
                return await query(collectionItemActor,  {"type": "project", search:{_id: args._id}, input: args.input }, 250);
            }else {
                return await query(collectionItemActor, {"type": "project", input: args.input}, 250);
            }

        },
    },

    query: {

        //Для корректной работы добавить полнотекстовый индекс db.project.createIndex( { name: "text" } )
        searchProjectByName: async (obj, args, context, info) => {

            const {collectionItemActor} = require("../../../actorSystem");

            return await query(collectionItemActor, {
                "type": "project",
                search: {
                    $text: {$search: args.name}
                }
            }, 250);

        },

        getProject: async (obj, args, context, info) => {


            const {collectionItemActor} = require("../../../actorSystem");

            return await query(collectionItemActor, {"type": "project", search: {_id: args._id}}, 250);


        },

        getProjects: async (obj, args, context, info) => {

            const {collectionActor} = require("../../../actorSystem");

            return await query(collectionActor, {"type": "project"}, 250);

        },
    }
  
}