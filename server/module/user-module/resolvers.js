import bcrypt from "bcrypt";

const { query } = require('nact');

const resource = "user";

module.exports = {

    query: {

        userInfo: async (obj, args, ctx, info) => {

            return ctx.user;

        },

        getCurrentUser: async (obj, args, ctx, info) => {

            let user = await ctx.user;



            // const {collectionItemActor} = require("../../actors/actorSystem");

            const collectionItemActor = ctx.children.get("item");
            return await query(collectionItemActor, {"type": "user", search: {_id: user._id}}, 250);

        },

        getUser: async (obj, args, ctx, info) => {

            // const {collectionItemActor} = require("../../actors/actorSystem");

            const collectionItemActor = ctx.children.get("item");
            return await query(collectionItemActor, {"type": "user", search: {_id: args._id}}, 250);

        },

        getUsers: async (obj, args, ctx, info) => {

            // const {collectionActor} = require("../../actors/actorSystem");


            const collectionActor = ctx.children.get("collection");

            return await query(collectionActor, {"type": "user"}, 250);

        },


        // getUserByExternalId: async (obj, args, ctx, info) => {
        //
        //     // const {collectionItemActor}  = require("../../actors/actorSystem");
        //     const collectionItemActor = ctx.children.get("item");
        //
        //     args.external_id = parseInt(args.external_id);
        //
        //     let user;
        //     switch (args.external_system) {
        //         case "telegram":
        //             user = await query(collectionItemActor, {"type": "user", search:
        //                     {telegram_id: args.external_id}
        //             }, 250);
        //             break;
        //         case "slack":
        //             user = await query(collectionItemActor, {"type": "user", search:
        //                     {slack_id: args.external_id}
        //             }, 250);
        //             break;
        //         case "vk":
        //             user = await query(collectionItemActor, {"type": "user", search:
        //                     {vk_id: args.external_id}
        //             }, 250);
        //             break;
        //
        //     }
        //
        //     return user;
        //
        // },

    },

    mutation:{

        registerUser: async (obj, args, ctx, info) => {

            // const { collectionItemActor} = require("../../actors/actorSystem");
            const collectionItemActor = ctx.children.get("item");

            if(args.input.password){
                const crypto_password = bcrypt.hashSync(args.input.password, 10);
                args.input.crypto_password = crypto_password;
                delete (args.input.password);
            }

            args.input.roles = ['user'];

            return await query(collectionItemActor, {type: "user", input: args.input}, 250);

        },

        changeUser: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const { collectionItemActor} = require("../../actors/actorSystem");

            return await query(collectionItemActor,  {type: "user", search:{_id: args._id}, input: args.input }, 250);


        },

        changeCurrentUser: async (obj, args, ctx, info) => {

            let current_user = await ctx.user;

            const collectionItemActor = ctx.children.get("item");
            // const { collectionItemActor} = require("../../actors/actorSystem");

            if (args.input.password) {
                let crypto_password = bcrypt.hashSync(args.input.password, 10);
                delete args.input.password;
                args.input.crypto_password = crypto_password;
                return await query(collectionItemActor, {type: "user", search:{_id: current_user._id}, input: args.input }, 250);
            }else{
                return await query(collectionItemActor, {type: "user", search:{_id: current_user._id}, input: args.input }, 250);
            }



        },

        changePerson: async (obj, args, ctx, info) => {

        },

        addIdentity: async (obj, args, ctx, info) => {

        },

        changeUserRoles: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const {collectionItemActor}  = require("../../actors/actorSystem");

            return await query(collectionItemActor, {type: "user", search: {_id: args._id}, input: {roles: args.roles }  }, 250);

        },

        connect: async (obj, args, ctx, info) => {

            let current_user = await ctx.user;

            const collectionItemActor = ctx.children.get("item");
            // const { collectionItemActor} = require("../../actors/actorSystem");

            let authenticator =  await query(collectionItemActor, {type: "authenticate_session", search: {"user_code": args.user_code} }, 250);

            let user;
            switch (authenticator.oob_channel) {
                case "slack":
                    user = await query(collectionItemActor, {type: "user", search: {"slack_id": authenticator.slack_id}  }, 250);
                    break;
                case "telegram":
                    user = await query(collectionItemActor, {type: "user", search: {"telegram_id": authenticator.telegram_id}  }, 250);
                    break;
                case "vk":
                    user = await query(collectionItemActor, {type: "user", search: {"vk_id": authenticator.vk_id}  }, 250);
                    break;
            }

            current_user = await query(collectionItemActor, {type: "user", search: {_id: current_user._id}, input:{
                    user_ids:[user._id],
                }}, 250);

            user = await query(collectionItemActor, {type: "user", search: {"_id": user._id} , input:{is_remove: true} }, 250);

            return user;

        }

    }


}