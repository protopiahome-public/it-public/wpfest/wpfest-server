import db from "./db";
import { readdirSync, readFileSync, statSync } from 'fs';
const path = require('path');
import {authenticateHandler} from "./oauth";

import resolvers from './resolvers';
import {ObjectId} from "promised-mongo";


//https://www.apollographql.com/docs/apollo-server/features/errors/

export default function(msg, ctx) {


    const empty_schema = readFileSync(__dirname + '/schema.graphqls', 'utf8');
    let typeDefs = [empty_schema];
    const dir = __dirname + "/module";
    const files = readdirSync(dir);
    for (const file of files) {
        const stat = statSync(path.join(dir, file));
        if (stat.isDirectory()){

            const schema = readFileSync(path.join(dir, file, "schema.graphqls"), "utf8");
            typeDefs.push(schema);
        }
    }


    const schema = {
        typeDefs,
        resolvers,
        formatError: (err) => {
            console.log(err);
            return err;
        },
        context: async ({  request, h }) => {

            const authenticate = await authenticateHandler(request, h, ctx);

            console.log(1);
            return {
                ...ctx,
                db: db,
                sid: authenticate
            }

        }
    };

    // export default schema;

    return schema;

}