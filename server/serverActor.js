import { readdirSync, existsSync, statSync } from 'fs';
const path = require('path');

const { spawnStateless} = require('nact');

import system from "./system";
import server from "./server";

const reset = async (msg, error, ctx) => {
    if(error){
        console.log(error);
    }
    // return ctx.reset;
};

const serverActor = spawnStateless(system, server ,'server', { onCrash: reset });

const actors1 = require("./actors/index.js");
actors1.default(serverActor);

const dir = __dirname + "/module";
const files = readdirSync(dir);
for (const file of files) {
    const stat = statSync(path.join(dir, file));
    if (stat.isDirectory()){
        // console.log(path.join(dir, file));
        if (existsSync(path.join(dir, file, "/actors/index.js"))){
            const actors = require(path.join(dir, file, "/actors/index.js"));
            actors.default(serverActor)
        }
    }
}

module.exports = {

    serverActor

}

//https://github.com/redux-utilities/flux-standard-action
//https://github.com/ncthbrt/nact/blob/master/test/actor.js
//https://medium.com/@allistersmith/diving-into-event-driven-web-apps-with-node-js-and-nact-7ecbf19ed41f

// const { MongoPersistenceEngine } = require('./mongo-persistence-engine');
// const connectionString = "mongodb://127.0.0.1:27017/test";
// const system = start(configurePersistence(new MongoPersistenceEngine(connectionString)));

// const spawnUserContactService = (parent, userId) => spawn(
//     parent,
// async function(msg, ctx){},
// userId
// );